#!/bin/bash

## Install the packages
yaourt -Syy --noconfirm $(cat ./packages.list);

## Copy the files
cp -Rlv ./etc/* /etc/;

## Enable the postfix and postgrey service
systemctl enable postfix.service postgrey.service;

## Start the postfix and postgrey service
systemctl start postfix.service postgrey.service;

## Send the message to the user
echo "NOTE:  Standalone installation requires modifying /etc/postfix/main.cf.";

