SELECT
	"Mail"."Alias"."Target" AS "destination"

FROM
	"Mail"."Alias"
	
WHERE
	LOWER("Mail"."Alias"."Source") = LOWER('%s')
		AND "Mail"."Alias"."Active" = true
;

