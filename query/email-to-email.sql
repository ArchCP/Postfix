SELECT
	("Mail"."Box"."User" || '@' || LOWER("Dns"."Domain"."Name")) AS "email"
	
FROM
	"Mail"."Box"
	
INNER JOIN
	"Dns"."Domain" ON (
		"Dns"."Domain"."ID" = "Mail"."Box"."DomainID"
	) 

WHERE
	(LOWER("Mail"."Box"."User") || '@' || LOWER("Dns"."Domain"."Name")) = LOWER('%s')
		AND "Mail"."Box"."Active" = true
;

