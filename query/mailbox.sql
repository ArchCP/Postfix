SELECT
	1
	
FROM
	"Mail"."Box"
	
INNER JOIN
	"Dns"."Domain" ON (
		"Dns"."Domain"."ID" = "Mail"."Box"."DomainID"
	)
	
WHERE
	("Mail"."Box"."User" || '@' || LOWER("Dns"."Domain"."Name")) = '%s'
		AND "Mail"."Box"."Active" = true
;

